var gulp = require('gulp');
var sync = require('browser-sync').create();
var sass = require('gulp-sass');
var minify = require('gulp-clean-css');
var concat = require('gulp-concat');
var path = require('path');
var uglify = require('gulp-uglify');

gulp.task('sass', function(){
	gulp.src('scss/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(minify())
		.pipe(gulp.dest('css'))
		.pipe(sync.stream());
});

gulp.task('unifyjs', function(){
	gulp.src(path.join('js/dev', '*.js'))
		.pipe(concat('main.js'))
		.pipe(uglify())
		.pipe(gulp.dest('js'));
});

gulp.task('default', ['sass', 'unifyjs'], function(){
	sync.init({
        server: "./"
    });
	
	gulp.watch("scss/**/*.scss", ['sass']);
    gulp.watch("*.html").on('change', sync.reload);
	gulp.watch(path.join('js/dev/', '*.js'), ['unifyjs']).on('change', sync.reload);
});